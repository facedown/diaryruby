class FormattedRecord
	def initialize(content, date = Time.now.strftime("%d.%m.%Y"))
		@content = content
		@date = date
	end

	def read
		"#{@date} - #{@content}\n"
	end
end