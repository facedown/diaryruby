class Diary
	def initialize(pages)
		@pages = pages
		@currentPage = @pages.first
	end

	def turnPage(number = nil)
		if(number.nil?)
			return turnPage(@currentPage.number + 1)
		end
		@pages.each do |page|
			if(page.number == number)
				return @currentPage = page
			end
		end
		raise Exception.new("Page #{number} does not exist")
	end

	def read
		@currentPage.content
	end

	def list
		content = ""
		@pages.each { |page| content.concat(page.content) }
		content
	end

	def write(record)
		@currentPage.write(record)
	end
end