class Page
	attr_reader :number

	def initialize(number, rows = 10)
		@number = number
		@rows = rows
		@records = Array.new
	end

	def content
		content = ""
		@records.each { |record| content.concat(record.read) }
		content
	end

	def write(record)
		if(@records.count == @rows)
			raise Exception.new("Page is full")
		end
		@records.push(record)
	end
end