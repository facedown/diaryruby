require "minitest/autorun"
require "../program/FormattedRecord"

class FormattedRecordTest < Minitest::Test
  def test_normal_reading
    assert_equal(
    	"04.07.2015 - Some text\n",
    	FormattedRecord.new("Some text", "04.07.2015").read
    )
  end

  def test_reading_with_implicit_date
  	assert_equal(
  		"#{Time.now.strftime("%d.%m.%Y")} - Some text\n",
  		FormattedRecord.new("Some text").read
  	)
  end
end