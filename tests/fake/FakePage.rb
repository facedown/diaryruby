class FakePage
	attr_reader :number
	
	def initialize(text, number)
		@text = text
		@number = number
	end

	def content
		@text
	end

	def write(record)
		@text.concat(record.read)
	end
end