class FakeRecord
	def initialize(text)
		@text = text
	end

	def read
		@text
	end
end