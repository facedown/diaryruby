require "minitest/autorun"
require "../program/Page"
require_relative "fake/FakeRecord"

class PageTest < Minitest::Test
  def setup
    @page = Page.new(1, 15)
  end

  def test_page_number
    assert_equal 1, @page.number
  end

  def test_content
  	@page.write(FakeRecord.new("First\n"))
  	@page.write(FakeRecord.new("Second"))
  	assert_equal "First\nSecond", @page.content
  end

  def test_writing
  	assert_raises(Exception) {
  		Page.new(1, 0).write(FakeRecord.new("First"))
  	}
  end
end