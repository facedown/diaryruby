require "minitest/autorun"
require "../program/Diary"
require_relative "fake/FakePage"
require_relative "fake/FakeRecord"

class DiaryTest < Minitest::Test
  def setup
    @diary = Diary.new([FakePage.new("One", 1), FakePage.new("Two", 2), FakePage.new("Three", 3)])
  end

  def test_reading
  	assert_equal "One", @diary.read
  end

  def test_turning
  	@diary.turnPage(2)
  	assert_equal "Two", @diary.read
  end

  def test_turning_by_step
    @diary.turnPage
    @diary.turnPage
    assert_equal "Three", @diary.read
  end

  def test_turning_to_unknown_page
  	assert_raises(Exception) {
  		@diary.turnPage(100)
  	}
  end

  def test_listing
    assert_equal "OneTwoThree", @diary.list
  end

  def test_writing
    @diary.write(FakeRecord.new("Four"))
    assert_equal "OneFour", @diary.read
  end
end